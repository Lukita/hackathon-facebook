<?php 
  //VERIFICA SE O USUÁRIO ESTÁ LOGADO E REDIRECIONA PARA TELA DE LOGIN CASO NÃO ESTEJA
  require_once "../controller/RedirecionaController.php";

  //CARREGA TODAS AS CLASSES QUE FORAM INSTANCIADAS
  function __autoload($class_name){
    require_once '../model/' . $class_name . '.php';
  } 

  $agendamento = new Agendamento;
    $_SESSION["id_cliente"];
   
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
	  <link rel="stylesheet" href="css/agenda_style.css">
	<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,900,700,500,300,100' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css">
    <title>Hackathon Facebook</title>
  </head>
  <body>
    <?php include_once "aside.php"; //BARRA LATEREAL ?>
    <?php include_once "header.php"; //BARRA SUPERIOR ?>
	<form id="cadastrar" action="../controller/AgendamentoController.php" method="post">

    <section class="corpo col-md-10 col-sm-8 col-xs-12">
      
      <div class="col-md-12 col-xs-12 col-sm-12 margin-left">
        <div class="conteudo">
          <div class="titulo">
            <i class="fa fa-list-alt" aria-hidden="true"></i> AGENDAMENTO
          </div>
          <div class="table-responsive">
            
    <div class="container">
      <div class="calendar">
        <div class="front">
          <div class="current-date">
            <h1>Fevereiro</h1>
            <h1>Março</h1>	
          </div>

          <div class="current-month">
            <ul class="week-days">
              <li>DOM</li>
              <li>SEG</li>
              <li>TER</li>
              <li>QUA</li>
              <li>QUI</li>
              <li>SEX</li>
              <li>SAB</li>
            </ul>

            <div class="weeks">
              <div class="first">
                <span class="last-month">28</span>
                <span class="last-month">29</span>
                <span class="last-month">30</span>
                <span class="last-month">31</span>
                <span>01</span>
                <span>02</span>
                <span>03</span>
              </div>

              <div class="second">
                <span>04</span>
                <span>05</span>
                <span class="event">06</span>
                <span>07</span>
                <span>08</span>
                <span>09</span>
                <span>10</span>
              </div>

              <div class="third">
                <span>11</span>
                <span>12</span>
                <span>13</span>
                <span>14</span>
                <span class="active">15</span>
                <span>16</span>
                <span>17</span>
              </div>

              <div class="fourth">
                <span>18</span>
                <span>19</span>
                <span>20</span>
                <span>21</span>
                <span>22</span>
                <span>23</span>
                <span>24</span>
              </div>
              <div class="fifth">
                <span>25</span>
                <span>26</span>
                <span>27</span>
                <span>28</span>
                <span>29</span>
                <span>30</span>
                <span>31</span>
              </div>
            </div>
          </div>
        </div>
        <div class="back">
          <input placeholder="">
          <div class="info">
            <div class="date">
              <p class="info-date">
              Data: <span><?php echo $agendamento->data; ?></span>
              </p>
              <p class="info-time">
              Hora: <span><?php echo $agendamento->hora; ?></span>
              </p>
            </div>
            <div class="observations">
              <p>
              Local de atendimento: <span><?php echo $agendamento->local; ?></span>
              </p>
            </div>
            <div class="observations">
              <p>
              Tipo de serviço: <span><?php echo $agendamento->servico; ?></span>
              </p>
            </div>
			 <div class="addresss">
              <p>
              Senha: <span><?php echo $agendamento->senha_atendimento; ?></span>
              </p>
            </div>
          </div>

          <div class="actions">
            <button class="save">
              Salvar <i class="ion-checkmark"></i>
            </button>
            <button class="dismiss">
              Sair <i class="ion-android-close"></i>
            </button>
          </div>
        </div>
</form>
      </div>
    </div>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js'></script>

    <script src="js/agenda_index.js"></script>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>

    

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js" integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>
    <script src="js/animation.js"></script>
  </body>
</html>