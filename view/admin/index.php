<?php 
  //CARREGA TODAS AS CLASSES QUE FORAM INSTANCIADAS
  function __autoload($class_name){
    require_once '../../model/' . $class_name . '.php';
  } 

  $acompanha = new Acompanhamento;
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">
    <title>Aguas Guariroba</title>
  </head>
  <body>
    <div class="container">
      <div class="admin">
        <div class="titulo">
          <i class="fa fa-list-alt" aria-hidden="true"></i> ORDENS DE SERVIÇOS
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>DATA</th>
                  <th>SERVIÇO</th>
                  <th>ORDEM</th>
                  <th>SITUAÇÃO</th>
                  <th colspan="2">OPÇÕES</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $resultado = $acompanha->listarTudo();

                  foreach ($resultado as $acompanha) {
                      $data = date("d/m/Y", strtotime($acompanha->data));
                ?>
                <tr>
                  <th scope="row"><?php echo $data; ?></th>
                  <td><?php echo $acompanha->nome; ?></td>
                  <td><?php echo $acompanha->ordem_servico; ?></td>
                  <td>
                    <?php
                      echo $acompanha->status=="Aberto" ? '<button type="button" class="btn btn-secondary btn-sm">ABERTO</button>': null;
                      echo $acompanha->status=="A caminho" ? '<button type="button" class="btn btn-secondary btn-sm">A CAMINHO</button>': null;
                      echo $acompanha->status=="Executando" ? '<button type="button" class="btn btn-warning btn-sm">EXECUTANDO...</button>': null;
                      echo $acompanha->status=="Não Executado" ? '<button type="button" class="btn btn-danger btn-sm">NÃO EXECUTADO</button>': null;
                      echo $acompanha->status=="Executado" ? '<button type="button" class="btn btn-secondary btn-sm">EXECUTADO</button>': null;
                      echo $acompanha->status=="Finalizado" ? '<button type="button" class="btn btn-success btn-sm">FINALIZADO</button>': null;
                    ?></td>
                  <td>
                    <form action="../../controller/SmsController.php" method="post">
                      <div class="col-xs-6">
                        <input type="hidden" name="id_acompanha" value="<?php echo $acompanha->id; ?>">
                        <select name="status" class="form-control btn-sm">
                          <?php
                            echo $acompanha->status=="Aberto" ? '
                            <option selected value="Aberto">Aberto</option>
                            <option value="A caminho">A caminho</option>
                            <option value="Executando">Executando</option>
                            <option>Não Executado</option>
                            <option>Executado</option>': null;
                            echo $acompanha->status=="A caminho" ? '
                            <option value="Aberto">Aberto</option>
                            <option selected value="A caminho">A caminho</option>
                            <option value="Executando">Executando</option>
                            <option>Não Executado</option>
                            <option>Executado</option>': null;
                            echo $acompanha->status=="Executando" ? '
                            <option value="Aberto">Aberto</option>
                            <option value="A caminho">A caminho</option>
                            <option selected value="Executando">Executando</option>
                            <option>Não Executado</option>
                            <option>Executado</option>
                            ': null;
                            echo $acompanha->status=="Não Executado" ? '
                            <option value="Aberto">Aberto</option>
                            <option value="A caminho">A caminho</option>
                            <option value="Executando">Executando</option>
                            <option>Executado</option>
                            <option selected>Não Executado</option>': null;
                            echo $acompanha->status=="Executado" ? '
                            <option value="Aberto">Aberto</option>
                            <option value="A caminho">A caminho</option>
                            <option value="Executando">Executando</option>
                            <option selected>Executado</option>
                            <option>Não Executado</option>': null;
                            echo $acompanha->status=="Finalizado" ? '
                            <option selected disabled>Finalizado</option>': null;
                          ?>
                          
                        </select>
                      </div>
                      <div class="col-xs-6">
                        <button type="submit" name="enviar_sms" class="btn btn-primary">CONFIRMAR</button>
                      </div>
                    </form>
                  </td>
                </tr>
                  <?php } ?>
              </tbody>
            </table>
      </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js" integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>
  </body>
</html>