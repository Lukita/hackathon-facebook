<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Hackathon Facebook</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js" integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>
  </head>
  <body>
  <script src="js/fb.js"></script>
    <div class="container">
      <section class="col-md-6 offset-md-3 flex-md-middle">
        <div class="login">
          <div id="status"></div>
          <?php if(isset($_GET["retorno"]) && $_GET["retorno"]=="sucesso"){ ?>
          	<div class="alert alert-success" role="alert">Sua conta foi criada com sucesso, faça o login abaixo:</div>
          <?php } ?>
          <form action="../controller/LoginController.php" method="post">
            <label>Matricula:</label>
            <input type="text" id="matricula" name="matricula" placeholder="123456-7" class="form-control">
            <br>
            <label>CPF:</label>
            <input type="text" id="cpf" name="cpf" placeholder="12345678910" class="form-control">
            <br>
            <div class="row">
	            <div class="col-md-6 col-sm-6 col-xs-12">
	            	<button type="submit" name="login" class="btn btn-success">ACESSAR</button>
	            </div>
	            <div class="col-md-6 col-sm-6 col-xs-12">
	            	<a href="cadastrar_cliente.php" class="btn btn-primary pull-right">CRIAR CONTA</a>
	            </div>
	        </div>
            
          	<h6 class="text-center margin-top-20">Acesse com o Facebook</h6>
          	<button id="fbLogin" class="btn btn-primary margin-top-20 btn-facebook" type="submit" onclick="checkLoginState()">
            	<div class="fb-login-button" data-max-rows="1" data-size="icon" data-show-faces="false" data-auto-logout-link="false"></div>
            	Entrar
          	</button>
          </form>
        </div>
      </section>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js" integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>
  </body>
</html>