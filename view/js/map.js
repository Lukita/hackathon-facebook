

//insere posição atual do usuario caso o navegador permita
latLng = {lat: -20.463783, lng: -54.615604}



//inicia mapa na aplicacao
function iniciarMapa() {
    var lat = latLng;
    var map = new google.maps.Map(document.getElementById('map'), {
        center: lat,
        zoom: 15,
        scrollwheel: false,
    });
    return map;
}

function insereMarcador(coordenada, mapa,descricao){

    str = '<h6>Problema já relatado</h6>' +
        '<p>'+descricao+'</p>';

    var janela = new google.maps.InfoWindow({
        content: str,
    });

    var marcador = new google.maps.Marker({
        position: coordenada,
        map: mapa,
        title: ''
    });

    marcador.addListener('click', function(){
        janela.open(mapa, marcador);
    })
}

function buscarEndereco(){
    var endereco = document.getElementById('endereco').value;

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({address: endereco}, function(results, status){
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                document.getElementById('txtlat').value = latitude;
                document.getElementById('txtlng').value = longitude;
            }
        }
    })
}

function contaMarcadores(idForm){
    var form = document.getElementById(idForm);
    var inputs = form.getElementsByTagName("input");
    var num = inputs.length;

    return (num / 2) - 1;
}
