-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 04-Dez-2016 às 15:19
-- Versão do servidor: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bd`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acompanhamento`
--

CREATE TABLE `acompanhamento` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `ordem_servico` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Aberto'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `acompanhamento`
--

INSERT INTO `acompanhamento` (`id`, `id_cliente`, `nome`, `data`, `hora`, `ordem_servico`, `status`) VALUES
(1, 1, 'Religamento de água', '2016-12-03', '09:22:00', '12345', 'Aberto'),
(2, 1, 'Religamento de água', '2016-12-03', '14:24:00', '13245', 'A caminho'),
(3, 1, 'Religamento de água', '2016-12-03', '15:16:00', '45216', 'Executando'),
(4, 1, 'Religamento de água', '2016-12-03', '15:25:00', '61320', 'Executado'),
(5, 1, 'Religamento de água', '2016-12-03', '15:57:00', '43671', 'Finalizado'),
(6, 1, 'Vazamento de esgoto', '2016-12-01', '07:15:00', '54321', 'Aberto'),
(7, 1, 'Vazamento de esgoto', '2016-12-01', '08:00:00', '15278', 'A caminho'),
(8, 1, 'Vazamento de esgoto', '2016-12-01', '08:20:00', '18778', 'Executando'),
(9, 1, 'Vazamento de esgoto', '2016-12-01', '08:40:00', '10098', 'Não Executado'),
(10, 1, 'Vazamento de esgoto', '2016-12-01', '09:00:00', '10035', 'Finalizado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `cpf` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `matricula` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `complemento` varchar(255) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `cep` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `cpf`, `email`, `matricula`, `endereco`, `cidade`, `complemento`, `bairro`, `cep`) VALUES
(1, 'Tiago Cardoso', '12345678910', 'tiago@tiago.com', '123456-7', 'Rua Almirante Barroso, 193', 'Campo Grande', NULL, 'Amambai', '12334566'),
(2, 'Lucas Duarte', '84726159300', 'lucas@lucas.com', '117264-0', 'Rua Das Araras, 174', 'Campo Grande', NULL, 'Aero Rancho', '184437320'),
(3, 'Gil Da Esfirra', '73625342166', 'gil@esfirra.com', '7654321-0', 'Rua Marechal Rondon, 54', 'Campo Grande', NULL, 'Vila Planalto', '760111388'),
(4, 'Igor HueHara', '76528432256', 'igor@hue.com', '16630-6', 'Av. Tamandaré, 1800', 'Campo Grande', NULL, 'Vila Nossa Senhora das Graças', '76447399'),
(5, 'Cleiton Goulart', '97463528744', 'cleiton@goulart.com', '16634-0', 'Rua Guaririba, 100', 'Campo Grande', NULL, 'Los Angeles', '49665100'),
(6, 'Henrique Galerito Junior', '09467645377', 'henrique@galerito.com', '14977-4', 'Rua Chapadão, 56', 'Campo Grande', NULL, 'São Francisco', '98667300');

-- --------------------------------------------------------

--
-- Estrutura da tabela `corte`
--

CREATE TABLE `corte` (
  `id` int(11) NOT NULL,
  `id_fatura` int(11) NOT NULL,
  `ordem_servico` varchar(255) NOT NULL,
  `data_entrada` date NOT NULL,
  `data_prevista` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `corte`
--

INSERT INTO `corte` (`id`, `id_fatura`, `ordem_servico`, `data_entrada`, `data_prevista`) VALUES
(1, 1, '', '2016-12-02', '2016-12-08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fatura`
--

CREATE TABLE `fatura` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `data` date NOT NULL,
  `vencimento` date NOT NULL,
  `valor` float NOT NULL,
  `consumo` varchar(255) NOT NULL,
  `situacao` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `fatura`
--

INSERT INTO `fatura` (`id`, `id_cliente`, `data`, `vencimento`, `valor`, `consumo`, `situacao`) VALUES
(1, 1, '2016-10-10', '2016-11-10', 151.55, '112 M³', 'Vencida'),
(2, 1, '2016-11-10', '2016-12-10', 350, '157 M³', 'Paga'),
(3, 1, '2016-12-10', '2016-10-10', 321.48, '212 M³', 'Vencida'),
(4, 3, '2016-11-10', '2016-12-10', 150, '115 M³', 'Paga'),
(5, 3, '2016-12-10', '2016-11-10', 79.47, '112 M³', 'Vencida'),
(6, 1, '2016-09-10', '2016-10-10', 700, '300 M³', 'Parcelada');

-- --------------------------------------------------------

--
-- Estrutura da tabela `nova_ligacao`
--

CREATE TABLE `nova_ligacao` (
  `id` int(11) NOT NULL,
  `mat_cliente` varchar(222) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `rg` varchar(255) NOT NULL,
  `orgao_exp_uf` varchar(255) DEFAULT NULL,
  `cpf_cnpj` varchar(255) NOT NULL,
  `profissao` varchar(255) NOT NULL,
  `nacionalidade` varchar(255) DEFAULT NULL,
  `estado_civil` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `cep` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `complemento` varchar(255) DEFAULT NULL,
  `pto_referencia` varchar(255) DEFAULT NULL,
  `ruas_paralelas` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tipo_imovel` varchar(255) NOT NULL,
  `situacao_imovel` varchar(255) NOT NULL,
  `tipo_rua` varchar(255) NOT NULL,
  `forma_pgto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `parcelamento`
--

CREATE TABLE `parcelamento` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `debito` float NOT NULL,
  `entrada` float NOT NULL,
  `parcelas` int(11) NOT NULL,
  `qtd` int(11) NOT NULL,
  `valor_parcelas` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `parcelamento`
--

INSERT INTO `parcelamento` (`id`, `id_cliente`, `debito`, `entrada`, `parcelas`, `qtd`, `valor_parcelas`) VALUES
(1, 1, 912.74, 273.822, 6, 0, 106.486),
(2, 1, 912.74, 273.822, 15, 0, 42.5945);

-- --------------------------------------------------------

--
-- Estrutura da tabela `vazamento`
--

CREATE TABLE `vazamento` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `lng` varchar(255) NOT NULL,
  `descricao` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `vazamento`
--

INSERT INTO `vazamento` (`id`, `id_cliente`, `lat`, `lng`, `descricao`) VALUES
(1, 1, '-20.456303', '-54.620288', 'Um hidrante foi ''atropelado'' aqui.'),
(2, 1, '-20.461753', '-54.631528', 'Vazamento subterrâneo no meio da passarela da Orla Morena.'),
(3, 1, '-20.464641', '-54.645822', 'Vazamento na rotatória dos quartéis com Av. Duque de Caxias.'),
(4, 1, '-20.464713', '-54.620029', 'Vazamento no Camelódromo com Av. Afonso Pena'),
(5, 1, '-20.466136', '-54.630486', 'Vazamento em frente à Assetur.'),
(6, 1, '-20.467487', '-54.634387', 'Vazamento na esquina da Av. Afonso Pena com Círculo Militar.'),
(7, 1, '-20.465740', '-54.625746', 'Vazamento na calçada em frente ao SENAI.'),
(8, 1, '-20.461331', '-54.617597', 'Boca de bueiro fora do Lugar e rua alagada.'),
(9, 1, '-20.458669', '-54.586481', 'Cano estourado alagando a rampa de acesso ao Shopping.'),
(11, 1, '-20.4567582', '-54.606166299999984', 'Vazamento canos');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acompanhamento`
--
ALTER TABLE `acompanhamento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente` (`id_cliente`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `matricula` (`matricula`);

--
-- Indexes for table `corte`
--
ALTER TABLE `corte`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_fatura` (`id_fatura`);

--
-- Indexes for table `fatura`
--
ALTER TABLE `fatura`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cliente` (`id_cliente`);

--
-- Indexes for table `nova_ligacao`
--
ALTER TABLE `nova_ligacao`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mat_cliente` (`mat_cliente`);

--
-- Indexes for table `parcelamento`
--
ALTER TABLE `parcelamento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente` (`id_cliente`);

--
-- Indexes for table `vazamento`
--
ALTER TABLE `vazamento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idcliente` (`id_cliente`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acompanhamento`
--
ALTER TABLE `acompanhamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `corte`
--
ALTER TABLE `corte`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fatura`
--
ALTER TABLE `fatura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `nova_ligacao`
--
ALTER TABLE `nova_ligacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `parcelamento`
--
ALTER TABLE `parcelamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vazamento`
--
ALTER TABLE `vazamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `acompanhamento`
--
ALTER TABLE `acompanhamento`
  ADD CONSTRAINT `acompanhamento_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`);

--
-- Limitadores para a tabela `corte`
--
ALTER TABLE `corte`
  ADD CONSTRAINT `corte_ibfk_1` FOREIGN KEY (`id_fatura`) REFERENCES `fatura` (`id`);

--
-- Limitadores para a tabela `fatura`
--
ALTER TABLE `fatura`
  ADD CONSTRAINT `fatura_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `fatura_ibfk_2` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `fk_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`);

--
-- Limitadores para a tabela `nova_ligacao`
--
ALTER TABLE `nova_ligacao`
  ADD CONSTRAINT `nova_ligacao_ibfk_1` FOREIGN KEY (`mat_cliente`) REFERENCES `cliente` (`matricula`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `parcelamento`
--
ALTER TABLE `parcelamento`
  ADD CONSTRAINT `parcelamento_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`);

--
-- Limitadores para a tabela `vazamento`
--
ALTER TABLE `vazamento`
  ADD CONSTRAINT `vazamento_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
