<?php
//VERIFICA SE O USUÁRIO ESTÁ LOGADO E REDIRECIONA PARA TELA DE LOGIN CASO NÃO ESTEJA
require_once "../controller/RedirecionaController.php";

//CARREGA TODAS AS CLASSES QUE FORAM INSTANCIADAS
function __autoload($class_name)
{
    require_once '../model/' . $class_name . '.php';
    //require('widget_cotacoes.php');
}

//INSTANCIA A CLASSE FUNCIONARIOS
$vazamento = new Vazamento;

$_SESSION["id_cliente"];
?>
<!DOCTYPE html>

<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css"
          integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Hackathon Facebook</title>
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDILdb_FWofJwSi6cnOtACFPo0_FjluIIU"></script>
</head>
<body>
<?php include_once "aside.php"; //BARRA LATEREAL ?>
<?php include_once "header.php"; //BARRA SUPERIOR ?>

<section class="corpo col-md-10 col-sm-8 col-xs-12">

    <section class="conteudo">
        <div class="titulo">
            <i class="fa fa-list-alt" aria-hidden="true"></i> Informar Vazamento
            <hr>
        </div>
        <form id="cadastrar" action="../controller/VazamentoController.php" method="post">
            <input type="hidden" value="<?php echo $_SESSION['id_cliente']; ?>" name="id_cliente">
            <fieldset>
                <legend>Relatar Vazamento</legend>
                <div class="row">
                    <div class="col-xs-12">
                        <label>Endereço:</label>
                        <input onchange="buscarEndereco()" type="text" id="endereco" placeholder="Ex. Av. Afonso Pena,1700 " class="form-control">
                        <input type="hidden" id="txtlat" value="" name="lat">
                        <input type="hidden" id="txtlng" value="" name="lng">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label>Descrição:</label>
                        <input type="text" placeholder="Ex. " class="form-control" name="descricao">
                    </div>
                </div>
            </fieldset>
            <br>
            <button type="submit" class="btn btn-lg btn-success" name="cadastrar">Cadastrar</button>
        </form>
    </section>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"
        integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js"
        integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js"
        integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK"
        crossorigin="anonymous"></script>
<script src="js/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script src="js/map.js"></script>
<script src="js/animation.js"></script>
</body>
</html>