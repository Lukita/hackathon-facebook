<?php
  //VERIFICA SE O USUÁRIO ESTÁ LOGADO E REDIRECIONA PARA TELA DE LOGIN CASO NÃO ESTEJA
  require_once "../controller/RedirecionaController.php";

  //CARREGA TODAS AS CLASSES QUE FORAM INSTANCIADAS
  function __autoload($class_name){
    require_once '../model/' . $class_name . '.php';
  }

  $acompanha = new Acompanhamento;
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Hackathon Facebook</title>
  </head>
  <body>
  <?php include_once "aside.php"; //BARRA LATEREAL ?>

    <?php include_once "header.php"; //BARRA SUPERIOR ?>

    <section class="corpo col-md-10 col-sm-8 col-xs-12">
      <section class="col-md-12 nopadding">
        <div class="conteudo">
          <?php
          if(isset($_GET["id"])){
            $resultado = $acompanha->rastreio($_GET["id"]);

          ?>
          <div class="titulo">
            <i class="fa fa-list-alt" aria-hidden="true"></i>ORDEM DE SERVIÇO Nº<?php echo $resultado->ordem_servico; ?>
          </div>
          <?php if($resultado->status=="Aberto"){ ?>
          <div class="stepwizard">
            <div class="stepwizard-row">
              <div class="stepwizard-step">
                <button type="button" class="btn btn-primary btn-circle"><i class="fa fa-map-marker" aria-hidden="true"></i></button>
                <p>Aberto</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-default btn-circle"><i class="fa fa-car" aria-hidden="true"></i></button>
                <p>A caminho</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-default btn-circle"><i class="fa fa-check" aria-hidden="true"></i></button>
                <p>Pendente</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-default btn-circle"><i class="fa fa-cog" aria-hidden="true"></i></button>
                <p>Finalizado</p>
              </div>
            </div>
          </div>
          <?php }else if($resultado->status=="A caminho"){ ?>
          <div class="stepwizard">
            <div class="stepwizard-row">
              <div class="stepwizard-step">
                <button type="button" class="btn btn-primary btn-circle"><i class="fa fa-map-marker" aria-hidden="true"></i></button>
                <p>Aberto</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-primary btn-circle"><i class="fa fa-car" aria-hidden="true"></i></button>
                <p>A caminho</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-default btn-circle"><i class="fa fa-check" aria-hidden="true"></i></button>
                <p>Pendente</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-default btn-circle"><i class="fa fa-cog" aria-hidden="true"></i></button>
                <p>Finalizado</p>
              </div>
            </div>
          </div>
          <?php }else if($resultado->status=="Executando"){ ?>
          <div class="stepwizard">
            <div class="stepwizard-row">
              <div class="stepwizard-step">
                <button type="button" class="btn btn-primary btn-circle"><i class="fa fa-map-marker" aria-hidden="true"></i></button>
                <p>Aberto</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-primary btn-circle"><i class="fa fa-car" aria-hidden="true"></i></button>
                <p>A caminho</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-warning btn-circle"><i class="fa fa-cog" aria-hidden="true"></i></button>
                <p>Executando...</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-default btn-circle"><i class="fa fa-cog" aria-hidden="true"></i></button>
                <p>Finalizado</p>
              </div>
            </div>
          </div>
          <?php }else if($resultado->status=="Não Executado"){ ?>
          <div class="stepwizard">
            <div class="stepwizard-row">
              <div class="stepwizard-step">
                <button type="button" class="btn btn-primary btn-circle"><i class="fa fa-map-marker" aria-hidden="true"></i></button>
                <p>Aberto</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-primary btn-circle"><i class="fa fa-car" aria-hidden="true"></i></button>
                <p>A caminho</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-danger btn-circle"><i class="fa fa-cog" aria-hidden="true"></i></button>
                <p>Serviço não executado,<br> saiba o que pode ter<br> acontecido e como<br> proceder <a href="">clicando aqui</a>.</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-default btn-circle"><i class="fa fa-cog" aria-hidden="true"></i></button>
                <p>Finalizado</p>
              </div>
            </div>
          </div>
          <?php }else if(($resultado->status=="Executado") || ($resultado->status=="Finalizado")){ ?>
          <div class="stepwizard">
            <div class="stepwizard-row">
              <div class="stepwizard-step">
                <button type="button" class="btn btn-primary btn-circle"><i class="fa fa-map-marker" aria-hidden="true"></i></button>
                <p>Aberto</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-primary btn-circle"><i class="fa fa-car" aria-hidden="true"></i></button>
                <p>A caminho</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-primary btn-circle"><i class="fa fa-cog" aria-hidden="true"></i></button>
                <p>Executado com sucesso!</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-success btn-circle"><i class="fa fa-cog" aria-hidden="true"></i></button>
                <p>Serviço finalizado,<br> avalie nosso atendimento<br> e serviço de manutenção<br> <a href="">clicando aqui</a>.</p>
              </div>
            </div>
          </div>
          <?php } ?>
        <?php }else{ ?>
            <div class="titulo">
              <i class="fa fa-list-alt" aria-hidden="true"></i> ACOMPANHAMENTO DE SERVIÇOS
            </div>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>DATA</th>
                    <th>SERVIÇO</th>
                    <th>ORDEM</th>
                    <th>SITUAÇÃO</th>
                    <th>OPÇÕES</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $resultado = $acompanha->listarAcompanhamentos($_SESSION["id_cliente"]);

                    foreach ($resultado as $acompanha) {
                        $data = date("d/m/Y", strtotime($acompanha->data));
                  ?>
                  <tr>
                    <th scope="row"><?php echo $data; ?></th>
                    <td><?php echo $acompanha->nome; ?></td>
                    <td><?php echo $acompanha->ordem_servico; ?></td>
                    <td>
                      <?php
                        echo $acompanha->status=="Aberto" ? '<button type="button" class="btn btn-secondary btn-sm">ABERTO</button>': null;
                        echo $acompanha->status=="A caminho" ? '<button type="button" class="btn btn-secondary btn-sm">A CAMINHO</button>': null;
                        echo $acompanha->status=="Executando" ? '<button type="button" class="btn btn-warning btn-sm">EXECUTANDO...</button>': null;
                        echo $acompanha->status=="Não Executado" ? '<button type="button" class="btn btn-danger btn-sm">NÃO EXECUTADO</button>': null;
                        echo $acompanha->status=="Executado" ? '<button type="button" class="btn btn-secondary btn-sm">EXECUTADO</button>': null;
                        echo $acompanha->status=="Finalizado" ? '<button type="button" class="btn btn-success btn-sm">FINALIZADO</button>': null;
                      ?></td>
                    <td>
                      <a href="acompanhamentos.php?id=<?php echo $acompanha->id; ?>" class="btn btn-primary btn-sm">ACOMPANHAR</a>
                    </td>
                  </tr>
                    <?php } ?>
                </tbody>
              </table>
            </div>
            <?php } ?>
        </div>
      </section>
    </section>


    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js" integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>
    <script src="js/animation.js"></script>
  </body>
</html>