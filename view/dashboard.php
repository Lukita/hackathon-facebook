<?php
//VERIFICA SE O USUÁRIO ESTÁ LOGADO E REDIRECIONA PARA TELA DE LOGIN CASO NÃO ESTEJA
require_once "../controller/RedirecionaController.php";

//CARREGA TODAS AS CLASSES QUE FORAM INSTANCIADAS
function __autoload($class_name)
{
    require_once '../model/' . $class_name . '.php';
}

$fatura = new Fatura;
$acompanha = new Acompanhamento;
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css"
          integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Hackathon Facebook</title>
</head>
<body>

<?php include_once "aside.php"; //BARRA LATEREAL ?>
<?php include_once "header.php"; //BARRA SUPERIOR ?>

<section class="corpo col-md-10 col-sm-8 col-xs-12">
    <?php
    $dados = new Corte();
    $id = $_SESSION['id_cliente'];
    $listaCorte = $dados->verificaCorte($id);
    if ($listaCorte != null) {
        echo '<div class="alert alert-danger fade in alert-dimissible" role="alert" style="margin-top: 10px; padding: 10px;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    <ul class="list-unstyled list-inline">
                        <li class="list-inline-item">
                            <i class="fa fa-exclamation-circle" style="font-size: 30px"></i>
                        </li>
                        <li class="list-inline-item">
                         <strong>Sr(a) ' . $listaCorte->nome . ',</strong> informamos que seu nome consta em nossa fila de corte
                referente a fatura vencida do    dia ' .date("d/m/Y", strtotime($listaCorte->vencimento)). ' e está sujeito ao corte.
                        </li>
                    </ul>
                <p>Por favor, <a href="consulta_debitos.php">Visite a consulta de débitos</a> para verificar suas faturas em atraso.</p>
            </div>';
    }
    ?>
    <section class="links-rapidos">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="consulta.php">
                    <div class="consultas box-link">
                        <div class="row">
                            <i class="fa fa-search icones-rapido" aria-hidden="true"></i>
                        </div>
                        <div class="row ">
                            CONSULTAS
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="acompanhamentos.php">
                    <div class="acompanhamentos box-link">
                        <div class="row">
                            <i class="fa fa-map-marker icones-rapido" aria-hidden="true"></i>
                        </div>
                        <div class="row ">
                            ACOMPANHAMENTOS
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="servicos.php">
                    <div class="agendamentos box-link">
                        <div class="row">
                            <i class="fa fa-cogs icones-rapido" aria-hidden="true"></i>
                        </div>
                        <div class="row ">
                            SERVIÇOS
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="informacao.php">
                    <div class="informacoes box-link">
                        <div class="row">
                            <i class="fa fa-plus-square-o icones-rapido" aria-hidden="true"></i>
                        </div>
                        <div class="row ">
                            INFORMAÇÕES
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>
    <div class="col-md-6 col-xs-12 col-sm-12 margin-left">
        <div class="conteudo">
            <div class="titulo">
                <i class="fa fa-list-alt" aria-hidden="true"></i> ÚLTIMAS FATURAS
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>VENCIMENTO</th>
                        <th>CONSUMO</th>
                        <th>VALOR</th>
                        <th>SITUAÇÃO</th>
                        <th>OPÇÕES</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $resultado = $fatura->listarFaturas($_SESSION["id_cliente"]);

                    foreach ($resultado as $faturas) {
                        $data = date("d/m/Y", strtotime($faturas->vencimento));
                        ?>
                        <tr>
                            <th scope="row"><?php echo $data; ?></th>
                            <td><?php echo $faturas->consumo; ?></td>
                            <td><?php echo $faturas->valor; ?></td>
                            <td>
                                <?php
                                echo $faturas->situacao == "Vencida" ? '<button type="button" class="btn btn-warning btn-sm min-width ">ATRASADA</button>' : null;
                                echo $faturas->situacao == "Aberta" ? '<button type="button" class="btn btn-primary btn-sm min-width">ABERTA</button>' : null;
                                echo $faturas->situacao == "Paga" ? '<button type="button" class="btn btn-success btn-sm min-width">PAGA</button>' : null;
                                ?></td>
                            <td>
                                <button type="button" class="btn btn-secondary btn-sm">2º VIA</button>
                                <button type="button" class="btn btn-primary btn-sm">DETALHES</button>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xs-12 col-sm-12 margin-right">
        <div class="conteudo">
            <div class="titulo">
                <i class="fa fa-list-alt" aria-hidden="true"></i> ACOMPANHAMENTOS
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>DATA</th>
                        <th>SERVIÇO</th>
                        <th>ORDEM</th>
                        <th>SITUAÇÃO</th>
                        <th>OPÇÕES</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $resultado = $acompanha->listarAcompanhamentos($_SESSION["id_cliente"]);

                    foreach ($resultado as $acompanha) {
                        $data = date("d/m/Y", strtotime($acompanha->data));
                        ?>
                        <tr>
                            <th scope="row"><?php echo $data; ?></th>
                            <td><?php echo $acompanha->nome; ?></td>
                            <td><?php echo $acompanha->ordem_servico; ?></td>
                            <td>
                                <?php
                                echo $acompanha->status == "Aberto" ? '<button type="button" class="btn btn-secondary btn-sm">ABERTO</button>' : null;
                                echo $acompanha->status == "A caminho" ? '<button type="button" class="btn btn-secondary btn-sm">A CAMINHO</button>' : null;
                                echo $acompanha->status == "Executando" ? '<button type="button" class="btn btn-warning btn-sm">EXECUTANDO...</button>' : null;
                                echo $acompanha->status == "Não Executado" ? '<button type="button" class="btn btn-danger btn-sm">NÃO EXECUTADO</button>' : null;
                                echo $acompanha->status == "Executado" ? '<button type="button" class="btn btn-secondary btn-sm">EXECUTADO</button>' : null;
                                echo $acompanha->status == "Finalizado" ? '<button type="button" class="btn btn-success btn-sm">FINALIZADO</button>' : null;
                                ?></td>
                            <td>
                                <a href="acompanhamentos.php?id=<?php echo $acompanha->id; ?>"
                                   class="btn btn-primary btn-sm">ACOMPANHAR</a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>


<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"
        integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js"
        integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js"
        integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK"
        crossorigin="anonymous"></script>
<script src="js/animation.js"></script>
</body>
</html>