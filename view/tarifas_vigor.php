<?php
//VERIFICA SE O USUÁRIO ESTÁ LOGADO E REDIRECIONA PARA TELA DE LOGIN CASO NÃO ESTEJA
require_once "../controller/RedirecionaController.php";

//CARREGA TODAS AS CLASSES QUE FORAM INSTANCIADAS
function __autoload($class_name)
{
    require_once '../model/' . $class_name . '.php';
}

$fatura = new Fatura;
$acompanha = new Acompanhamento;
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css"
          integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Hackathon Facebook</title>
</head>
<body>
<?php include_once "aside.php" ?>

<?php include_once "header.php"; //BARRA SUPERIOR ?>

<section class="corpo col-md-10 col-sm-8 col-xs-12">
    <section class="links-rapidos">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="servicos_prazos.php">
                    <div class="informacoes box-link">
                        <div class="row">
                            <i class="fa fa-file-text-o icones-rapido" aria-hidden="true"></i>
                        </div>
                        <div class="row ">
                            SERVIÇOS E PRAZOS
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="religacao_agua.php">
                    <div class="informacoes box-link">
                        <div class="row">
                            <i class="fa fa-bath icones-rapido" aria-hidden="true"></i>
                        </div>
                        <div class="row ">RELIGAÇÃO DE ÁGUA</div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="tarifas_vigor.php">
                    <div class="informacoes box-link">
                        <div class="row">
                            <i class="fa fa-credit-card icones-rapido" aria-hidden="true"></i>
                        </div>
                        <div class="row ">
                            TARIFAS EM VIGOR
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="tarifa_social.php">
                    <div class="informacoes box-link">
                        <div class="row">
                            <i class="fa fa-usd icones-rapido" aria-hidden="true"></i>
                        </div>
                        <div class="row ">
                            TARIFA SOCIAL
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>
    <div class="col-md-12 col-xs-12 col-sm-12 margin-left">
        <div class="conteudo">
            <div class="titulo">
                <i class="fa fa-list-alt" aria-hidden="true"></i> TARIFAS EM VIGOR
            </div>
            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>Estrutura Tarifária*</th>
                        <th>Faixa de Consumo</th>
                        <th>Tarifa de Água (R$/m³)</th>
                        <th>Tarifa de Esgoto (R$/m³)</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Tarifa Social</td>
                    <td>Até 20 m³</td>
                    <td>1,84</td>
                    <td>1,29</td>
                </tr>
                <tr>
                    <td>Residencial</td>
                    <td>0 a 10 m³<br/>
                        11 a 15 m³<br/>
                        16 a 20 m³<br/>
                        21 a 25 m³<br/>
                        26 a 30 m³<br/>
                        31 a 50 m³<br/>
                        Acima 50 m³
                    </td>
                    <td>4,07<br/>
                        5,20<br/>
                        5,30<br/>
                        5,84<br/>
                        7,20<br/>
                        8,63<br/>
                        9,49
                    </td>
                    <td>2,85<br/>
                        3,64<br/>
                        3,71<br/>
                        4,09<br/>
                        5,04<br/>
                        6,04<br/>
                        6,64
                    </td>
                </tr>
                <tr>
                    <td>Comercial</td>
                    <td>0 a 10 m³<br/>
                        Acima de 10 m³
                    </td>
                    <td>5,60<br/>
                        11,48
                    </td>
                    <td>3,92<br/>
                        8,03
                    </td>
                </tr>
                <tr>
                    <td>Industrial</td>
                    <td>0 a 10 m³<br/>
                        Acima de 10 m³
                    </td>
                    <td style="text-align: center;">8,76<br/>
                        16,85
                    </td>
                    <td style="text-align: center;">6,13<br/>
                        11,80
                    </td>
                </tr>
                <tr>
                    <td>Poder Público</td>
                    <td>0 a 20 m³<br/>
                        Acima de 20 m³
                    </td>
                    <td>5,10<br/>
                        21,13
                    </td>
                    <td>3,57<br/>
                        14,79
                    </td>
                </tr>
                </tbody>
            </table>
            <p><strong>*Decreto Municipal n. 12.761, de 2 de dezembro de 2015 publicado no Diário Oficial/CG-MS de
                    03/12/2015.</strong></p>

            </tbody>
            </table>
        </div>
    </div>
    </div>
</section>


<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"
        integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js"
        integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js"
        integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK"
        crossorigin="anonymous"></script>
<script src="js/animation.js"></script>
</body>
</html>