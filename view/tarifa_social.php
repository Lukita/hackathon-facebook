<?php
//VERIFICA SE O USUÁRIO ESTÁ LOGADO E REDIRECIONA PARA TELA DE LOGIN CASO NÃO ESTEJA
require_once "../controller/RedirecionaController.php";

//CARREGA TODAS AS CLASSES QUE FORAM INSTANCIADAS
function __autoload($class_name)
{
    require_once '../model/' . $class_name . '.php';
}

$fatura = new Fatura;
$acompanha = new Acompanhamento;
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css"
          integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Hackathon Facebook</title>
</head>
<body>
<?php include_once "aside.php" ?>
<?php include_once "header.php"; //BARRA SUPERIOR ?>

<section class="corpo col-md-10 col-sm-8 col-xs-12">
    <section class="links-rapidos">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="servicos_prazos.php">
                    <div class="informacoes box-link">
                        <div class="row">
                            <i class="fa fa-file-text-o icones-rapido" aria-hidden="true"></i>
                        </div>
                        <div class="row ">
                            SERVIÇOS E PRAZOS
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="religacao_agua.php">
                    <div class="informacoes box-link">
                        <div class="row">
                            <i class="fa fa-bath icones-rapido" aria-hidden="true"></i>
                        </div>
                        <div class="row ">RELIGAÇÃO DE ÁGUA</div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="tarifas_vigor.php">
                    <div class="informacoes box-link">
                        <div class="row">
                            <i class="fa fa-credit-card icones-rapido" aria-hidden="true"></i>
                        </div>
                        <div class="row ">
                            TARIFAS EM VIGOR
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="tarifa_social.php">
                    <div class="informacoes box-link">
                        <div class="row">
                            <i class="fa fa-usd icones-rapido" aria-hidden="true"></i>
                        </div>
                        <div class="row ">
                            TARIFA SOCIAL
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>
    <div class="col-md-12 col-xs-12 col-sm-12 margin-left">
        <div class="conteudo">
            <div class="titulo">
                <i class="fa fa-list-alt" aria-hidden="true"></i> TARIFA SOCIAL
            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>VOLUME (m³)</th>
                        <th>VALOR ÁGUA (R$/m³)</th>
                        <th>VALOR ESGOTO (R$/m³)</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Até 20 m³</td>
                    <td>1,84</td>
                    <td>1,29</td>
                </tr>
                </tbody>
            </table>
            <div>Tem direito à tarifa social quem:</div>
            <div></div>
            <div>
                <p><strong>01.</strong> Possuir renda familiar não superior a 1 (um) salário mínimo, a qual deverá ser
                    comprovada mediante apresentação da Carteira de Trabalho e Previdência Social, Guia de Recolhimento
                    para a Previdência Social ou outro documento equivalente;<br/>
                    <strong>02.</strong> Ser proprietário de um único imóvel destinado exclusivamente à sua moradia e de
                    sua família, desde que isento do pagamento do IPTU nos termos da Lei Municipal n. 2.786/90, com as
                    alterações da Lei n.2.950/93;<br/>
                    <strong>03.</strong> Ser consumidor monofásico de energia elétrica, cujo consumo não poderá
                    ultrapassar 100 Kwh/mês;<br/>
                    <strong>04.</strong> Não consumir mais do que 20m³/mês de água.</p>
            </div>
            <div>
                <p>Para ser beneficiado com a tarifa social, deverá o usuário fazer seu cadastramento junto à
                    concessionária Águas Guariroba S/A, comprovando o preenchimento dos requisitos exigidos nos incisos
                    I, II, II e IV.</p>
                <p>Não poderão ser cadastrados os usuários que se encontrarem na condição de inadimplentes junto à
                    concessionária.</p>
                <p>Anualmente, todos os beneficiados com a tarifa social deverão comparecer perante a concessionária
                    Águas Guariroba S/A, para renovar o seu cadastramento, devendo na oportunidade apresentar a mesma
                    documentação para comprovar a continuidade de seu enquadramento nas condições exigidas.</p>
                <p>A tarifa social foi instituída pela Lei nº 3.928, de 26 de dezembro de 2001.</p>
            </div>
        </div>

        </tbody>
        </table>
    </div>
    </div>
    </div>
</section>


<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"
        integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js"
        integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js"
        integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK"
        crossorigin="anonymous"></script>
<script src="js/animation.js"></script>
</body>
</html>