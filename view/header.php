<header class="col-md-10 col-sm-8 col-xs-12">
  <nav class="navbar navbar-light">
      <ul class="nav navbar-nav">
        <li class="nav-item canais">
          <a class="nav-link" href="#">CANAIS DE ATENDIMENTO</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"><i class="fa fa-phone" aria-hidden="true"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        </li>
      </ul>
      <p id="cliente"></p>
      <ul class="nav navbar-nav float-lg-right">
        <li class="nav-item">
          <a class="nav-link" href="#"><i class="fa fa-address-card-o" aria-hidden="true"></i> ATUALIZAR DADOS</a>
        </li><li class="nav-item">
          <a class="nav-link" href="../controller/LoginController.php?status=sair"><i class="fa fa-sign-out" aria-hidden="true"></i> SAIR</a>
        </li>
      </ul>
  </nav>
</header>
