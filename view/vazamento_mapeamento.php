<?php

//VERIFICA SE O USUÁRIO ESTÁ LOGADO E REDIRECIONA PARA TELA DE LOGIN CASO NÃO ESTEJA
require_once "../controller/RedirecionaController.php";
//CARREGA TODAS AS CLASSES QUE FORAM INSTANCIADAS
function __autoload($class_name)
{
    require_once '../model/' . $class_name . '.php';
    //require('widget_cotacoes.php');
}

//INSTANCIA A CLASSE FUNCIONARIOS
$vazamento = new Vazamento;
$_SESSION["id_cliente"];
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/styleMap.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="js/map.js"></script>
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDILdb_FWofJwSi6cnOtACFPo0_FjluIIU"></script>
</head>
<body>
<?php include_once "aside.php"; //BARRA LATEREAL ?>
<?php include_once "header.php"; //BARRA SUPERIOR ?>
<section class="corpo col-md-10 col-sm-8 col-xs-12">
    <section class="conteudo container">
        <div class="titulo">
            <i class="fa fa-list-alt" aria-hidden="true"></i> VAZAMENTOS RELATADOS <br>
            <small>Vazamentos relatados pela população em sua região</small>
            <hr>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="map">
                </div>
                <div class="margin-top-20">
                    <a href="vazamento.php">
                        <button type="submit" class="btn btn-lg btn-success">Informar Vazamento</button>
                    </a>
                </div>
                <form action="../controller/VazamentoController.php" method="post" id="mapForm">

                    <?php $i = 0;
                    $resultado = $vazamento->listarTudo($_SESSION["id_cliente"]);
                    foreach ($resultado as $vazamento) { ?>

                        <!--inicio coordenada-->
                        <input type="hidden" value="<?php echo $vazamento->lat; ?>" id="latitude<?php echo $i; ?>"
                               name="lat">
                        <input type="hidden" value="<?php echo $vazamento->lng; ?>" id="longitude<?php echo $i; ?>"
                               name="lng">
                        <!--fim coordenada-->
                        <?php $i++;
                    } ?>
                </form>
                <form action="../controller/VazamentoController.php" method="post" id="descMark">
                    <?php $i = 0;
                    foreach ($resultado as $vazamento) { ?>

                        <!--inicio coordenada-->
                        <input type="hidden" value="<?php echo $vazamento->descricao; ?>"
                               id="descricao<?php echo $i; ?>">
                        <!--fim coordenada-->
                        <?php $i++;
                    } ?>
                </form>


            </div>
        </div>
    </section>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"
        integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js"
        integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js"
        integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK"
        crossorigin="anonymous"></script>
<script>
    var mapa = iniciarMapa();
    var count = contaMarcadores("mapForm");
    while (count >= 0) {
        var coordenada = {
            lat: parseFloat(document.getElementById('latitude' + count).value),
            lng: parseFloat(document.getElementById('longitude' + count).value)
        }
        var descricao = document.getElementById('descricao' + count).value;
        insereMarcador(coordenada, mapa, descricao);
        count--;
    }
</script>
<script src="js/animation.js"></script>
</body>
</html>