<?php 
  //VERIFICA SE O USUÁRIO ESTÁ LOGADO E REDIRECIONA PARA TELA DE LOGIN CASO NÃO ESTEJA
  require_once "../controller/RedirecionaController.php";

  //CARREGA TODAS AS CLASSES QUE FORAM INSTANCIADAS
  function __autoload($class_name){
    require_once '../model/' . $class_name . '.php';
  } 

  $fatura = new Fatura;
  $acompanha = new Acompanhamento;
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Hackathon Facebook</title>
  </head>
  <body>
    <?php include_once "aside.php"?>

    <?php include_once "header.php"; //BARRA SUPERIOR ?>

    <section class="corpo col-md-10 col-sm-8 col-xs-12">
      <section class="links-rapidos">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="servicos_prazos.php">
              <div class="informacoes box-link">
                <div class="row">
                  <i class="fa fa-file-text-o icones-rapido" aria-hidden="true"></i>
                </div>
                <div class="row ">
                  SERVIÇOS E PRAZOS
                </div>
              </div>
            </a>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="religacao_agua.php">
              <div class="informacoes box-link">
                <div class="row">
                  <i class="fa fa-bath icones-rapido" aria-hidden="true"></i>
                </div>
                <div class="row ">RELIGAÇÃO DE ÁGUA</div>
              </div>
            </a>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="tarifas_vigor.php">
              <div class="informacoes box-link">
                <div class="row">
                  <i class="fa fa-credit-card icones-rapido" aria-hidden="true"></i>
                </div>
                <div class="row ">
                  TARIFAS EM VIGOR
                </div>
              </div>
            </a>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="tarifa_social.php">
              <div class="informacoes box-link">
                <div class="row">
                  <i class="fa fa-usd icones-rapido" aria-hidden="true"></i>
                </div>
                <div class="row ">
                  TARIFA SOCIAL
                </div>
              </div>
            </a>
          </div>
        </div>
      </section>
      <div class="col-md-12 col-xs-12 col-sm-12 margin-left">
        <div class="conteudo">
          <div class="titulo">
            <i class="fa fa-list-alt" aria-hidden="true"></i> RELIGAÇÃO DE ÁGUA
          </div>
      <p>Em caso de corte por débito, o pedido de religação deve ser feito através de nosso teleatedimento 0800 642 0115 ou em uma de nossas lojas de atendimento.</p>
<p>Caso a religação seja feita após consumo final, o cliente deve ir a uma loja com os seguintes documentos:</p>
<p>· Inquilino: Para solicitar religação de água, o inquilino deverá procurar o Atendimento munido do contrato de locação reconhecido firma em cartório;</p>
<p>· Proprietário: Caso o solicitante seja proprietário do imóvel, ele deve fazer o pedido munido da escritura do imóvel e/ou IPTU, Contrato de compra e venda reconhecida firma em cartório;</p>
	  
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>

    

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js" integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>
    <script src="js/animation.js"></script>
  </body>
</html>