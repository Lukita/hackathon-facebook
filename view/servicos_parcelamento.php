<?php
//VERIFICA SE O USUÁRIO ESTÁ LOGADO E REDIRECIONA PARA TELA DE LOGIN CASO NÃO ESTEJA
require_once "../controller/RedirecionaController.php";

//CARREGA TODAS AS CLASSES QUE FORAM INSTANCIADAS
function __autoload($class_name){
    require_once '../model/' . $class_name . '.php';
}

$fatura = new Fatura;
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Hackathon Facebook</title>
</head>
<body>
<?php include_once "aside.php"; //BARRA LATEREAL ?>
<?php include_once "header.php"; //BARRA SUPERIOR ?>

<section class="corpo col-md-10 col-sm-8 col-xs-12">
    <div class="col-md-12 col-xs-12 col-sm-12 margin-left">
        <div class="conteudo">
            <div class="titulo">
                <i class="fa fa-list-alt" aria-hidden="true"></i> PARCELAMENTO
            </div>

            <?php
            $resultado = $fatura->contarFaturasVencidas();
            $entrada = $resultado->total * 0.3;
            $entrada = number_format($entrada, 2 , ',', '.');
            $total = number_format($resultado->total, 2 , ',', '.');


            if(!isset($_GET["parcelas"])){ ?>
                <div class="row">
                    <div class="col-md-12">
                        <h6>Regras do parcelamento</h6>
                        <ul>
                            <li>É necessário uma entrada no valor de 30% do débito total.</li>
                            <li>Você pode parcelar em até 24 vezes.</li>
                        </ul>
                    </div>
                </div>
                <form action="../controller/ParcelamentoController.php" method="post">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Você tem um débito no valor de (R$):</label>
                            <input type="text" name="debito" class="form-control" value="<?php echo $total; ?>" readonly>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                        <label>A entrada será de (R$):</label>
                            <input type="text" name="entrada" class="form-control" value="<?php echo $entrada; ?>" readonly>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Escolha a quantidade de parcelas (Mínimo de 1, Máximo de 24):</label>
                            <input type="number" name="parcelas" class="form-control" min="1" max="24">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary" name="calcular_parcelamento">CALCULAR PARCELAS</button>
                        </div>
                    </div>
                </form>


            <?php }else{
            if($resultado->qtd>=2){

            ?>
            <form action="../controller/ParcelamentoController.php" method="post">
                <input type="hidden" name="id_cliente" value="<?php echo $_SESSION['id_cliente'] ?>">
                <div class="row">
                    <div class="col-md-12">
                        <label>Você tem um débito no valor de (R$):</label>
                        <input type="text" name="debito" class="form-control" value="<?php echo $total; ?>" readonly>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <label>A entrada será de (R$):</label>
                        <input type="text" name="entrada" class="form-control" value="<?php echo $entrada; ?>" readonly>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <label>Quantidade de parcelas (Mínimo de 1, Máximo de 24):</label>
                        <input type="number" name="parcelas" class="form-control" value="<?php echo $_GET['qtd']; ?>" readonly>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <label>Valor das parcelas (R$):</label>
                        <input type="number" name="valor_parcelas" class="form-control" value="<?php echo $_GET['parcelas']; ?>" readonly>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success" name="parcelar">ACEITAR PARCELAMENTO</button>
                    </div>
                </div>
            </form>
            <?php } ?>
            <?php } ?>
        </div>
    </div>
</section>



<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js" integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>
<script src="js/animation.js"></script>
</body>
</html>