<?php
//VERIFICA SE O USUÁRIO ESTÁ LOGADO E REDIRECIONA PARA TELA DE LOGIN CASO NÃO ESTEJA
require_once "../controller/RedirecionaController.php";

//CARREGA TODAS AS CLASSES QUE FORAM INSTANCIADAS
function __autoload($class_name){
    require_once '../model/' . $class_name . '.php';
}

$fatura = new Fatura;
$acompanha = new Acompanhamento;
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Hackathon Facebook</title>
</head>
<body>
<?php include_once "aside.php"; //BARRA LATEREAL ?>
<?php include_once "header.php"; //BARRA SUPERIOR ?>

<section class="corpo col-md-10 col-sm-8 col-xs-12">
    <div class="col-md-12 col-xs-12 col-sm-12 margin-left">
        <div class="conteudo">
            <div class="titulo">
                <i class="fa fa-list-alt" aria-hidden="true"></i> DÉBITOS<br>
                <small>Consulte seus débitos</small>
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <td>Código</td>
                            <td>Vencimento</td>
                            <td>Consumo</td>
                            <td>Valor</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        //instancia um objeto fatura para verificar as faturas vencidas
                        $faturas = new Fatura();
                        $id = $_SESSION['id_cliente'];
                        $dados = $faturas->listarFaturasVencidas($id);
                        $total = 0;

                        foreach($dados as $debito){
                            echo '<tr>';
                            echo '<td>'.$debito->id.'</td>';
                            echo '<td>'.date("d/m/Y", strtotime($debito->vencimento)).'</td>';
                            echo '<td>'.$debito->consumo.'</td>';
                            echo '<td>'.$debito->valor.' R$</td>';
                            echo '</tr>';
                            $total+= $debito->valor;}
                            $total = number_format($total, 2 , ',', '.');
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4">
                                <strong class="pull-right">Total:R$ <?php echo $total;?></strong>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>



<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js" integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>
<script src="js/animation.js"></script>
</body>
</html>