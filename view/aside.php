<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '746406938861793',
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>
<aside class="col-md-2 col-sm-4 col-xs-12 nopadding" id="sidebar">
  <ul class="nopadding">
    <li class="margin-top-20">
      <a href="dashboard.php">
        <div class="menu">
          <div class="row">
            <i class="fa fa-home icones-rapido" aria-hidden="true"></i>
          </div>
          <div class="row">
            INÍCIO
          </div>
        </div>
      </a>
    </li>
    <li>
      <a href="consulta.php">
        <div class="menu">
          <div class="row">
            <i class="fa fa-search icones-rapido" aria-hidden="true"></i>
          </div>
          <div class="row ">
            CONSULTAS
          </div>
        </div>
      </a>
    </li>
    <li>
      <a href="acompanhamentos.php">
        <div class="menu">
          <div class="row">
            <i class="fa fa-map-marker icones-rapido" aria-hidden="true"></i>
          </div>
          <div class="row ">
            ACOMPANHAMENTOS
          </div>
        </div>
      </a>
    </li>
    <li>
      <a href="servicos.php">
        <div class="menu">
          <div class="row">
            <i class="fa fa-cogs icones-rapido" aria-hidden="true"></i>
          </div>
          <div class="row ">
            SERVIÇOS
          </div>
        </div>
      </a>
    </li>
    <li>
      <a href="vazamento_mapeamento.php">
        <div class="menu">
          <div class="row">
            <i class="fa fa-exclamation-triangle icones-rapido" aria-hidden="true"></i>
          </div>
          <div class="row ">
            RELATAR VAZAMENTOS
          </div>
        </div>
      </a>
    </li>
    <li>
      <a href="informacao.php">
        <div class="menu">
          <div class="row">
            <i class="fa fa-plus-square-o icones-rapido" aria-hidden="true"></i>
          </div>
          <div class="row ">
            INFORMAÇÕES
          </div>
        </div>
      </a>
    </li>
  </ul>
</aside>
