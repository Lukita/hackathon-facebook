<?php
	function __autoload($class_name){
		require_once '../model/' . $class_name . '.php';
	}

  	$corte = new Corte;
  	$corte->setId_fatura($_POST['id_fatura']);
	$corte->setOrdem_servico($_POST['ordem_servico']);
  	$corte->setData_servico($_POST['data_servico']);
	$corte->setData_entrada($_POST['data_entrada']);
	$corte->setData_prevista($_POST['data_prevista']);

			
  	if(isset($_POST['cadastrar'])){
      
    	if($corte->inserir()){
      		header("location: ../view/corte.php");
      	}else{
      		header("location: ../view/corte.php");
      	}
    }

    if(isset($_POST['atualizar'])){
    	$id = $_POST['id'];
    	if($corte->atualizar($id)){
	        header("location: ../view/corte.php");
      	}else{
			header("location: ../view/corte.php");
		}
	}

?>
