<?php
        //VERIFICA SE O USUÁRIO ESTÁ LOGADO E REDIRECIONA PARA TELA DE LOGIN CASO NÃO ESTEJA
          require_once "../controller/RedirecionaController.php";

          //CARREGA TODAS AS CLASSES QUE FORAM INSTANCIADAS
          function __autoload($class_name){
            require_once '../model/' . $class_name . '.php';
          } 

        if(isset($_POST["enviar_sms"])){

                $cliente = new Cliente;
                $sms = new Sms;
                $acompanha = new Acompanhamento;

                $acompanha_result = $acompanha->listarPorID($_POST["id_acompanha"]);
                $resultado = $cliente->listarPorID($_SESSION["id_cliente"]);

                if(!empty($acompanha_result)){
                        $acompanha->setId_cliente($acompanha_result->id_cliente);
                        $acompanha->setNome($acompanha_result->nome);
                        $acompanha->setData($acompanha_result->data);
                        $acompanha->setHora($acompanha_result->hora);
                        $acompanha->setOrdem_servico($acompanha_result->ordem_servico);
                        $acompanha->setStatus($_POST["status"]);

                        $acompanha->atualizar($acompanha_result->id);
                }
                

                if($_POST["status"]=="A caminho"){
                        $mensagem = "Bom dia, um técnico está a caminho da sua residência para executar o serviço solicitado, aguarde...";
                }else if($_POST["status"]=="Executando"){
                        $mensagem = "O técnico está na sua residência para executar o serviço solicitado, qualquer dúvida pergunte-o ou entre em contato conosco.";
                }else if($_POST["status"]=="Executado"){
                        $mensagem = "O serviço foi executado com sucesso, caso o problema persista não exite em nos contactar, você pode avaliar nosso atendimento e suporte técnico <a href='#'>clicando aqui</a>";
                }else if($_POST["status"]=="Não executado"){
                        $mensagem = "O serviço não pôde ser executado, isso pode acontecer por diversos motivos acesse nosso site para saber o que aconteceu e como proceder <a href='#'>clicando aqui</a>";
                }      	

        	$sms->setTelefone_destino("55" . $resultado->tel);
        	$sms->setTelefone_origem("5567992397242");
        	$sms->setTipo("texto");
        	$sms->setMensagem($mensagem);
        	$sms->setFormato("JSON");
        	$sms->setToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuZGlyZWN0Y2FsbHNvZnQuY29tIiwiYXVkIjoiMTkyLjE2OC4yMzMuODIiLCJpYXQiOjE0ODA4MDcyNDksIm5iZiI6MTQ4MDgwNzI0OSwiZXhwIjoxNDgwODEwODQ5LCJjbGllbnRfb2F1dGhfaWQiOiI0OTgyMyJ9.kiogYtrvnFedmetbehY55d1YzzK9nZoSL1V8Ts1z1uw");

                $dados = $sms->enviarSms();

        	if(!empty($dados)){
        		header("Location: ../view/admin/index.php");
        	}else{
        		echo "deu ruim meu irmão";
        	}
        }

        
?>