<?php
	session_start();

	function __autoload($class_name){
		require_once '../model/' . $class_name . '.php';
	}

  	$cliente = new Cliente;

  	//RECEBE VALORES DO FORMULÁRIO DE LOGIN E OS FILTRA
  	$matricula = filter_input(INPUT_POST, "matricula", FILTER_SANITIZE_MAGIC_QUOTES);
  	$cpf = filter_input(INPUT_POST, "cpf", FILTER_SANITIZE_MAGIC_QUOTES);

  	//VERIFICA SE HÁ SOLICITAÇÃO DE LOGIN E O TIPO DE USUÁRIO QUE ESTÁ A SOLICITANDO
	if(isset($_POST['login'])){
		$cliente->setMatricula($matricula); 
		$cliente->setCpf($cpf);

		//SE AS INFORMAÇÕES BATEREM REDIRECIONA PARA A PÁGINA DE DASHBOARD SENÃO RETORNA A TELA DE LOGIN COM ERRO DE SENHA/EMAIL
		if($cliente->logarCliente()){
			header("Location: ../view/dashboard.php");
		}else{
			header("Location: ../view/index.php?retorno=login_incorreto");
		}
	}

	//DESLOGA O USUÁRIO
	if(isset($_GET['status']) && $_GET['status']=="sair"){
		$cliente->deslogarCliente();
		header("Location: ../view/index.php");
	}

?>
