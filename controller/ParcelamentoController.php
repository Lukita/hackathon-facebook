<?php
	function __autoload($class_name){
		require_once '../model/' . $class_name . '.php';
	}

  $parcelamento = new Parcelamento;
		
	if(isset($_POST["calcular_parcelamento"])){
    $parcelas = ($_POST["debito"]-$_POST["entrada"]) / $_POST["parcelas"];

    header("Location: ../view/servicos_parcelamento.php?parcelas=".$parcelas."&qtd=".$_POST["parcelas"]);
  }

  if(isset($_POST["parcelar"])){
    $parcelamento->setId_cliente($_POST["id_cliente"]);
    $parcelamento->setDebito($_POST["debito"]);
    $parcelamento->setEntrada($_POST["entrada"]);
    $parcelamento->setParcelas($_POST["parcelas"]);
    $parcelamento->setValor_parcelas($_POST["valor_parcelas"]);

    if($parcelamento->inserir()){
      header("Location: ../view/consulta_parcelamento.php");
    }
  }

?>
