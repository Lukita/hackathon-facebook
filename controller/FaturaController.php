<?php
	function __autoload($class_name){
		require_once '../model/' . $class_name . '.php';
	}

  	$fatura = new Fatura;
		
		
	$id_cliente, $data,$valor,$consumo,$situacao
  	$fatura->setId_cliente($_POST['id_cliente']);
	$fatura->setData($_POST['data']);
  	$fatura->setValor($_POST['valor']);
	$fatura->setConsumo($_POST['consumo']);
	$fatura->setVencimento($_POST['vencimento']);
	$fatura->setSituacao($_POST['situacao']);
			
  	if(isset($_POST['cadastrar'])){
      
    	if($fatura->inserir()){
      		header("location: ../views/fatura.php?retorno=sucesso");
      	}else{
      		header("location: ../views/fatura.php?retorno=falha");
      	}
    }

    if(isset($_POST['atualizar'])){
    	$id = $_POST['id'];
    	if($fatura->atualizar($id)){
	        header("location: ../views/fatura.php?retorno=sucesso");
      	}else{
			header("location: ../views/fatura.php?retorno=falha");
		}
	}

?>
