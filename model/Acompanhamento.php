<?php

require_once 'Molde.php';

class Acompanhamento extends Molde {
   protected $tabela = "acompanhamento"; //NOME DA TABELA
   private $id_cliente, $nome, $data, $hora, $ordem_servico, $status;
   
    function setId_cliente($id_cliente) {
       $this->id_cliente = $id_cliente;
   }

   function setNome($nome) {
       $this->nome = $nome;
   }

   function setData($data) {
       $this->data = $data;
   }

   function setHora($hora) {
       $this->hora = $hora;
   }

   function setOrdem_servico($ordem_servico) {
       $this->ordem_servico = $ordem_servico;
   }

   function setStatus($status) {
       $this->status = $status;
   }

  
    public function inserir() {
        //INSERT
        $sql = "INSERT INTO $this->tabela   (id_cliente, nome, data, hora, ordem_servico, status)
                                            VALUES 
                                            (:id_cliente, :nome, :data, :hora, :ordem_servico, :status)";
        
        //CONEXAO COM O BANCO
        $executa = BancoDados::prepare($sql);

        //VALIDA PARAMETROS
        $executa->bindParam(":id_cliente", $this->id_cliente);
        $executa->bindParam(":nome", $this->nome);
		    $executa->bindParam(":data", $this->data);
        $executa->bindParam(":hora", $this->hora);
        $executa->bindParam(":ordem_servico", $this->ordem_servico); 
        $executa->bindParam(":status", $this->status);        
      
        //RETORNA A EXECUÇÃO
        return $executa->execute();
    }
    
    public function atualizar($id) {
        //UPDATE
        $sql = "UPDATE $this->tabela SET id_cliente = :id_cliente, nome = :nome, data = :data, hora = :hora, ordem_servico = :ordem_servico, status = :status WHERE  id = :id ";
        
        //CONEXAO COM O BANCO
		
        $executa = BancoDados::prepare($sql);
        
        //VALIDA PARAMETROS
		    $executa->bindParam(":id",$id);
        $executa->bindParam(":id_cliente", $this->id_cliente);
        $executa->bindParam(":nome", $this->nome);
		    $executa->bindParam(":data", $this->data);
        $executa->bindParam(":hora", $this->hora);
		    $executa->bindParam(":ordem_servico", $this->ordem_servico);
        $executa->bindParam(":status", $this->status);
		
		
        
        //RETORNA A EXECUÇÃO
        return $executa->execute();
    
  
    }

    public function listarAcompanhamentos($id){ //METODO DE LISTAGEM POR ID
        $sql = "SELECT * FROM $this->tabela WHERE id_cliente = :id ORDER BY data DESC";
        $executa = BancoDados::prepare($sql);
        $executa->bindParam(':id', $id, PDO::PARAM_INT);
        $executa->execute();
        return $executa->fetchAll();
    }

    public function rastreio($id){
      $sql = "SELECT * FROM $this->tabela WHERE id = :id";
        $executa = BancoDados::prepare($sql);
        $executa->bindParam(':id', $id, PDO::PARAM_INT);
        $executa->execute();
        return $executa->fetch();
    }
}
