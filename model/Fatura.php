<?php

require_once 'Molde.php';

class Fatura extends Molde {
    protected $tabela = "fatura"; //NOME DA TABELA
   private $id_cliente, $data,$valor,$consumo,$situacao;
  
   function getId_cliente() {
       return $this->id_cliente;
   }//oi
   
   function getData() {
       return $this->data;
   }

   function getValor() {
       return $this->valor;
   }

   function getConsumo() {
       return $this->consumo;
   }

   function getSituacao() {
       return $this->situacao;
   }
   
   function getVencimento() {
       return $this->vencimento;
   }
   
    function setVencimento($vencimento) {
       $this->vencimento = $vencimento;
   }

   function setId_cliente($id_cliente) {
       $this->id_cliente = $id_cliente;
   }

   function setData($data) {
       $this->data = $data;
   }

   function setValor($valor) {
       $this->valor = $valor;
   }

   function setConsumo($consumo) {
       $this->consumo = $consumo;
   }

   function setSituacao($situacao) {
       $this->situacao = $situacao;
   }

  
    public function inserir() {
        //INSERT
        $sql = "INSERT INTO $this->tabela   (id_cliente,data,valor,consumo,situacao)
                                            VALUES 
                                            (:id_cliente,:data,:valor,:consumo,:situacao)";
        
        //CONEXAO COM O BANCO
        $executa = BancoDados::prepare($sql);

        //VALIDA PARAMETROS
        $executa->bindParam(":id_cliente", $this->id_cliente);
		    $executa->bindParam(":data", $this->data);
        $executa->bindParam(":valor", $this->valor);
        $executa->bindParam(":consumo", $this->consumo); 
        $executa->bindParam(":situacao", $this->situacao);        
      
        //RETORNA A EXECUÇÃO
        return $executa->execute();
    }
    
    public function atualizar($id) {
        //UPDATE
        $sql = "UPDATE $this->tabela SET id_cliente = :id_cliente,data = :data,valor = :valor,consumo = :consumo,situacao = :situacao;
                               		WHERE  id = :id ";
        
        //CONEXAO COM O BANCO
		
        $executa = BancoDados::prepare($sql);
        
        //VALIDA PARAMETROS
		    $executa->bindParam(":id",$id);
        $executa->bindParam(":id_cliente", $this->id_cliente);
		    $executa->bindParam(":data", $this->data);
        $executa->bindParam(":valor", $this->valor);
		    $executa->bindParam(":consumo", $this->consumo);
        $executa->bindParam(":situacao", $this->situacao);
		
		
        
        //RETORNA A EXECUÇÃO
        return $executa->execute();
    
  
    }

    public function listarFaturas($id){ //METODO DE LISTAGEM POR ID
        $sql = "SELECT * FROM $this->tabela WHERE id_cliente = :id ORDER BY data DESC";
        $executa = BancoDados::prepare($sql);
        $executa->bindParam(':id', $id, PDO::PARAM_INT);
        $executa->execute();
        return $executa->fetchAll();
    }

    public function listarFaturasVencidas($id){ //METODO DE LISTAGEM POR ID DE FATURAS VENCIDAS
        $sql = "SELECT * FROM $this->tabela WHERE id_cliente = :id AND situacao = 'Vencida' ORDER BY data DESC";
        $executa = BancoDados::prepare($sql);
        $executa->bindParam(':id', $id, PDO::PARAM_INT);
        $executa->execute();
        return $executa->fetchAll();
    }

    public function contarFaturasVencidas(){
        $sql = "SELECT SUM(valor) as 'total', COUNT(valor) as 'qtd' FROM $this->tabela WHERE situacao LIKE 'Vencida' GROUP BY situacao";
        $executa = BancoDados::prepare($sql);
        $executa->execute();
        return $executa->fetch();
    }
}
