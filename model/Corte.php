<?php

require_once 'Molde.php';

class Corte extends Molde
{
    protected $tabela = "corte"; //NOME DA TABELA
    private $id_fatura, $ordem_servico, $data_servico, $data_entrada, $data_prevista;

    function getId_fatura()
    {
        return $this->id_fatura;
    }

    function getOrdem_servico()
    {
        return $this->ordem_servico;
    }

    function getData_servico()
    {
        return $this->data_servico;
    }

    function getData_entrada()
    {
        return $this->data_entrada;
    }

    function getData_prevista()
    {
        return $this->data_prevista;
    }

    function setId_fatura($id_fatura)
    {
        $this->id_fatura = $id_fatura;
    }

    function setOrdem_servico($ordem_servico)
    {
        $this->ordem_servico = $ordem_servico;
    }

    function setData_servico($data_servico)
    {
        $this->data_servico = $data_servico;
    }

    function setData_entrada($data_entrada)
    {
        $this->data_entrada = $data_entrada;
    }

    function setData_prevista($data_prevista)
    {
        $this->data_prevista = $data_prevista;
    }


    //$   $id_fatura,$ordem_servico,$data_servico,$data_entrada,$data_prevista
    public function inserir()
    {
        //INSERT
        $sql = "INSERT INTO $this->tabela   (id_fatura,ordem_servico,data_servico,data_entrada,data_prevista)
                                            VALUES 
                                            (:id_fatura,:ordem_servico,:data_servico,:data_entrada,:data_prevista)";

        //CONEXAO COM O BANCO
        $executa = BancoDados::prepare($sql);

        //VALIDA PARAMETROS    
        $executa->bindParam(":ordem_servico", $this->ordem_servico);
        $executa->bindParam(":data_servico", $this->data_servico);
        $executa->bindParam(":data_entrada", $this->data_entrada);
        $executa->bindParam(":data_prevista", $this->data_prevista);
        $executa->bindParam(":id_fatura", $this->id_fatura);
        $executa->bindParam(":ordem_servico", $this->ordem_servico);
        $executa->bindParam(":data_servico", $this->data_servico);
        $executa->bindParam(":data_entrada", $this->data_entrada);
        $executa->bindParam(":data_prevista", $this->data_prevista);


        //RETORNA A EXECUÇÃO
        return $executa->execute();
    }

    public function atualizar($id)
    { //id_cliente, data,hora,tipo_servico,local_atendimento,senha_atendimento
        //UPDATE
        $sql = "UPDATE $this->tabela SET id_fatura= :id_fatura,ordem_servico = :ordem_servico,data_servico = :data_servico,data_entrada = :data_entrada,data_prevista = :data_prevista
                               		WHERE  id = :id ";

        //CONEXAO COM O BANCO

        $executa = BancoDados::prepare($sql);

        //VALIDA PARAMETROS
        $executa->bindParam(":id", $id);
        $executa->bindParam(":ordem_servico", $this->ordem_servico);
        $executa->bindParam(":data_servico", $this->data_servico);
        $executa->bindParam(":data_entrada", $this->data_entrada);
        $executa->bindParam(":data_prevista", $this->data_prevista);
        $executa->bindParam(":id_fatura", $this->id_fatura);
        $executa->bindParam(":ordem_servico", $this->ordem_servico);
        $executa->bindParam(":data_servico", $this->data_servico);
        $executa->bindParam(":data_entrada", $this->data_entrada);
        $executa->bindParam(":data_prevista", $this->data_prevista);

        //RETORNA A EXECUÇÃO
        return $executa->execute();


    }

    public function verificaCorte($id)
    {
        $query = "SELECT c.data_entrada, c.data_prevista, cl.nome, f.id, f.vencimento FROM $this->tabela c
JOIN fatura f on c.id_fatura = f.id
JOIN cliente cl on cl.id = f.id_cliente  WHERE f.id_cliente = $id;";
        $executa = BancoDados::prepare($query);
        $executa->execute();
        return $executa->fetch();
    }
}
