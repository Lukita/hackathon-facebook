<?php

require_once 'Molde.php';

class Cliente extends Molde {
  protected $tabela = "cliente"; //NOME DA TABELA
  private $id,$nome,$cpf,$email,$matricula,$endereco,$cidade,$complemento,$bairro,$cep;
   
   function getId() {
       return $this->id;
   }

   function getNome() {
       return $this->nome;
   }

   function getCpf() {
       return $this->cpf;
   }

   function getEmail() {
       return $this->email;
   }

   function getMatricula() {
       return $this->matricula;
   }

   function getEndereco() {
       return $this->endereco;
   }

   function getCidade() {
       return $this->cidade;
   }

   function getComplemento() {
       return $this->complemento;
   }

   function getBairro() {
       return $this->bairro;
   }

   function getCep() {
       return $this->cep;
   }

   function setId($id) {
       $this->id = $id;
   }

   function setNome($nome) {
       $this->nome = $nome;
   }

   function setCpf($cpf) {
       $this->cpf = $cpf;
   }

   function setEmail($email) {
       $this->email = $email;
   }

   function setMatricula($matricula) {
       $this->matricula = $matricula;
   }

   function setEndereco($endereco) {
       $this->endereco = $endereco;
   }

   function setCidade($cidade) {
       $this->cidade = $cidade;
   }

   function setComplemento($complemento) {
       $this->complemento = $complemento;
   }

   function setBairro($bairro) {
       $this->bairro = $bairro;
   }

   function setCep($cep) {
       $this->cep = $cep;
   }
  
    public function inserir() {   
        //INSERT
        $sql = "INSERT INTO $this->tabela   (nome,cpf,email,matricula,endereco,cidade,complemento,bairro)
                                            VALUES 
                                            (:nome,:cpf,:email,:matricula,:endereco,:cidade,:complemento,:bairro)";
        
        //CONEXAO COM O BANCO
        $executa = BancoDados::prepare($sql);

        //VALIDA PARAMETROS//
		    $executa->bindParam(":nome", $this->nome);
        $executa->bindParam(":cpf", $this->cpf);
        $executa->bindParam(":email", $this->email);
		    $executa->bindParam(":matricula", $this->matricula);
        $executa->bindParam(":endereco", $this->endereco);
        $executa->bindParam(":cidade", $this->cidade); 
		    $executa->bindParam(":complemento", $this->complemento);
        $executa->bindParam(":bairro", $this->bairro);

        //RETORNA A EXECUÇÃO
        return $executa->execute();
    }
    
    public function atualizar($id) { //id,nome,cpf,email,matricula,endereco,cidade,complemento,bairro,cep
        //UPDATE
        $sql = "UPDATE $this->tabela SET id = :id,nome = :nome,cpf = :cpf,email = :email,matricula = :matricula,
									endereco = :endereco,cidade = :cidade,complemento = :complemento,bairro = :bairro,cep = :cep;
                               		WHERE  id = :id ";
        
        //CONEXAO COM O BANCO
		
        $executa = BancoDados::prepare($sql);
        
        //VALIDA PARAMETROS    
		    $executa->bindParam(":id",$id);
        $executa->bindParam(":nome", $this->nome);
		    $executa->bindParam(":cpf", $this->cpf);
        $executa->bindParam(":email", $this->email);
    		$executa->bindParam(":matricula", $this->matricula);
    		$executa->bindParam(":endereco", $this->endereco);
    		$executa->bindParam(":cidade", $this->cidade);
    		$executa->bindParam(":complemento", $this->complemento);
    		$executa->bindParam(":bairro", $this->bairro);
    		$executa->bindParam(":cep", $this->cep);

        //RETORNA A EXECUÇÃO
        return $executa->execute();
    }
    public function logarCliente(){
        //SELECT USUÁRIO DONO
        $sql = "SELECT * FROM $this->tabela WHERE matricula = :matricula";
        
        //CONEXÃO COM O BANCO
        $executa = BancoDados::prepare($sql);
        
        //VALIDA PARAMETROS
        $executa->bindParam(":matricula", $this->matricula);
        $executa->execute();

        //FAZ A VERIFICAÇÃO
        if($executa->rowCount() == 1){
            $dados = $executa->fetch(PDO::FETCH_OBJ);
            if($this->cpf==$dados->cpf){
                $_SESSION['id_cliente'] = $dados->id;
                $_SESSION['usuario'] = $dados->nome;
                $_SESSION['logado'] = true;

                return true;
            }
        }else{
            return false;
        }
    }
    
    public function deslogarCliente(){
        if(isset($_SESSION['logado'])){
            unset($_SESSION['logado']);
            session_destroy();
        }
    }
}
