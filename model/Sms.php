<?php 
	class Sms {
		private $telefone_destino;
		private $telefone_origem;
		private $tipo;
		private $mensagem;
		private $formato;
		private $token;
		private $url = "https://api.directcallsoft.com/sms/send";

		public function setTelefone_destino($telefone_destino){
			$this->telefone_destino = $telefone_destino;
		}
		public function setTelefone_origem($telefone_origem){
			$this->telefone_origem = $telefone_origem;
		}
		public function setTipo($tipo){
			$this->tipo = $tipo;
		}
		public function setMensagem($mensagem){
			$this->mensagem = $mensagem;
		}
		public function setFormato($formato){
			$this->formato = $formato;
		}
		public function setToken($token){
			$this->token = $token;
		}

		public function enviarSms(){
			$data = http_build_query(array('origem'=>$this->telefone_origem, 'destino'=>$this->telefone_destino, 'tipo'=>$this->tipo, 'access_token'=>$this->token, 'texto'=>$this->mensagem));

			$ch =   curl_init();
	        curl_setopt($ch, CURLOPT_URL, $this->url);
	        curl_setopt($ch, CURLOPT_POST, true);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	        curl_setopt($ch, CURLOPT_HEADER, 0);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	        
	        $return = curl_exec($ch);
	        
	        curl_close($ch);
	        
	        // Converte os dados de JSON para ARRAY
	        return json_decode($return, true);
		}
	}

?>