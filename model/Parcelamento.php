<?php

require_once 'Molde.php';

class Parcelamento extends Molde {
  protected $tabela = "parcelamento"; //NOME DA TABELA
  private  $id_cliente, $debito, $entrada, $parcelas, $valor_parcelas;
   

  function setId_cliente($id_cliente) {
    $this->id_cliente = $id_cliente;
  }

  function setDebito($debito) {
    $this->debito = $debito;
  }

  function setEntrada($entrada) {
    $this->entrada = $entrada;
  }

  function setParcelas($parcelas) {
    $this->parcelas = $parcelas;
  }

  function setValor_parcelas($valor_parcelas) {
    $this->valor_parcelas = $valor_parcelas;
  }

  public function inserir() {   
      //INSERT
      $sql = "INSERT INTO $this->tabela   (id_cliente,debito,entrada, parcelas, valor_parcelas)
                                          VALUES 
                                          (:id_cliente,:debito,:entrada, :parcelas, :valor_parcelas)";
      
      //CONEXAO COM O BANCO
      $executa = BancoDados::prepare($sql);

      //VALIDA PARAMETROS    
      $executa->bindParam(":id_cliente", $this->id_cliente);
      $executa->bindParam(":debito", $this->debito);
      $executa->bindParam(":entrada", $this->entrada); 
      $executa->bindParam(":parcelas", $this->parcelas);  
      $executa->bindParam(":valor_parcelas", $this->valor_parcelas); 
        
    
      //RETORNA A EXECUÇÃO
      return $executa->execute();
  }
  public function atualizar($id){

  }
  public function listarParcelamento($id){ //METODO DE LISTAGEM POR ID
        $sql = "SELECT * FROM $this->tabela ";
        $executa = BancoDados::prepare($sql);
        $executa->bindParam(':id', $id, PDO::PARAM_INT);
        $executa->execute();
        return $executa->fetchAll();
    }
}
