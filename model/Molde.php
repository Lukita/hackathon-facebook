<?php

require_once 'BancoDados.php';

abstract class Molde extends BancoDados {
    protected $tabela; //NOME DA TABELA
    
    abstract public function inserir(); //METODO DE INSERÇÃO
    abstract public function atualizar($id); //METODO DE ATUALIZAÇÃO
    
    public function listarPorID($id){ //METODO DE LISTAGEM POR ID
        $sql = "SELECT * FROM $this->tabela WHERE id = :id";
        $executa = BancoDados::prepare($sql);
        $executa->bindParam(':id', $id, PDO::PARAM_INT);
        $executa->execute();
        return $executa->fetch();
    }
    
    public function listarTudo(){ //METODO DE LISTAGEM COMPLETA
        $sql = "SELECT * FROM $this->tabela";
        $executa = BancoDados::prepare($sql);
        $executa->execute();
        return $executa->fetchAll();
    }
    
    public function deletar($id){ //METODO DE REMOÇÃO
        $sql = "DELETE FROM $this->tabela WHERE id = :id";
        $executa = BancoDados::prepare($sql);
        $executa->bindParam(':id', $id, PDO::PARAM_INT);
        return $executa->execute();
    }
    
}
