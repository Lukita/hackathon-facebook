<?php

require_once 'Molde.php';

class Vazamento extends Molde {
    protected $tabela = "vazamento"; //NOME DA TABELA
   private $id_cliente,$lat,$lng,$descricao;

   

   function getId_cliente() {
       return $this->id_cliente;
   }

   function getLat() {
       return $this->lat;
   }

   function getLng() {
       return $this->lng;
   }

   function getDescricao() {
       return $this->descricao;
   }


   function setId_cliente($id_cliente) {
       $this->id_cliente = $id_cliente;
   }

   function setLat($lat) {
       $this->lat = $lat;
   }

   function setLng($lng) {
       $this->lng = $lng;
   }

   function setDescricao($descricao) {
       $this->descricao = $descricao;
   }

   
        
    public function inserir() {    //$id,$id_cliente,$lat,$lng,$descricao
        //INSERT
        $sql = "INSERT INTO $this->tabela   (id_cliente,lat,lng,descricao)
                                            VALUES 
                                            (:id_cliente,:lat,:lng,:descricao)";
        
        //CONEXAO COM O BANCO
        $executa = BancoDados::prepare($sql);

        //VALIDA PARAMETROS    
        $executa->bindParam(":id_cliente", $this->id_cliente);
		$executa->bindParam(":lat", $this->lat);
        $executa->bindParam(":lng", $this->lng);
        $executa->bindParam(":descricao", $this->descricao); 
          
      
        //RETORNA A EXECUÇÃO
        return $executa->execute();
    }
    
    public function atualizar($id) { //id_cliente,total,qtd_parcela,valor_parcela
        //UPDATE
        $sql = "UPDATE $this->tabela SET id_cliente = :id_cliente,lat = :lat,lng = :lng,descricao = :descricao;
                               		WHERE  id = :id ";
        
        //CONEXAO COM O BANCO
		
        $executa = BancoDados::prepare($sql);
        
        //VALIDA PARAMETROS  
		$executa->bindParam(":id",$id);
        $executa->bindParam(":id_cliente", $this->id_cliente);
		$executa->bindParam(":lat", $this->lat);
        $executa->bindParam(":lng", $this->lng);
		$executa->bindParam(":descricao", $this->descricao);

		
		
        
        //RETORNA A EXECUÇÃO
        return $executa->execute();
    
  
    }
}
