<?php
//CONFIGS DE CONEXÃO
define('DB_HOST', 'localhost');
define('DB_NAME', 'bd');
define('DB_USER', 'root');
//dedine('DB_PASS', '');
    
class BancoDados {
    private static $instance;
    
    public static function getInstance() {
        if(!isset(self::$instance)){
            try {
                self::$instance = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8', DB_USER, '');
                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);    
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }
        
        return self::$instance;
    }
    
    public static function prepare($sql) {
        return self::getInstance()->prepare($sql);
    }
}
